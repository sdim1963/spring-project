package com.example.demo.controllers;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@Slf4j
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    public UserController() {
        LOGGER.info("UserController");
    }

    @GetMapping(value = "/")
    public ResponseEntity<String> home() {
        LOGGER.info("Hello from my spring boot application");
        return new ResponseEntity<>("Hello from my spring boot application", HttpStatus.OK);

    }

    @GetMapping(value = "/users")
    public ResponseEntity<String> getUsers() {
        LOGGER.info("Hello from /users");
        return new ResponseEntity<>("Hello from /users", HttpStatus.OK);
    }




}
